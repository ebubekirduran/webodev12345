﻿using g139010027_Proje.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace g139010027_Proje.Controllers
{
    public class HomeController : Controller
    {
        OtickDbEntities db = new OtickDbEntities();

        
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult ProjeEkle()
        {
            return View();
        }

        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang); return Redirect(returnUrl);

        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Hakkimizda()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Iletisim()
        {
           

            return View();
        }
        public ActionResult _SonYazilarIndex()
        {
            var Res = (from o in db.Yazilar.OrderByDescending(o => o.YaziID).Take(3).ToList()

                       select new Yazilar()
                       {
                           YaziID = o.YaziID,
                           YaziBaslik = o.YaziBaslik,
                           YaziIcerik = o.YaziIcerik,
                           YaziResim = o.YaziResim,
                           YaziTarih = o.YaziTarih

                       }).ToList();

            return View(Res);
        }




        public ActionResult _SonProjelerIndex()
        {
            var Res = (from o in db.Projeler.OrderByDescending(o => o.ProjeID).Take(3).ToList()

                       select new Projeler()
                       {
                           ProjeID = o.ProjeID,
                           ProjeAdi = o.ProjeAdi,
                           ProjeAciklama = o.ProjeAciklama,
                           ProjeResim = o.ProjeResim,
                           ProjeTarih = o.ProjeTarih

                       }).ToList();

            return View(Res);
        }



        [HttpGet]
        [Route("Home/Yazilar/?sayfa=1")]
        public ActionResult Yazilar(int sayfa)
        {

            var Res = (from o in db.Yazilar.OrderByDescending(o => o.YaziID).ToList()

                       select new Yazilar()
                       {
                           YaziID = o.YaziID,
                           YaziBaslik = o.YaziBaslik,
                           YaziIcerik = o.YaziIcerik,
                           YaziResim = o.YaziResim,
                           YaziTarih = o.YaziTarih

                       }).ToPagedList(sayfa, 6);


            return View(Res);


        }




        [Authorize]
        [HttpGet]
        [Route("Home/Projeler/?sayfa=1")]
        public ActionResult Projeler(int sayfa)
        {

            var Res = (from o in db.Projeler.OrderByDescending(o => o.ProjeID).ToList()

                       select new Projeler()
                       {
                           ProjeID = o.ProjeID,
                           ProjeAdi = o.ProjeAdi,
                           ProjeAciklama = o.ProjeAciklama,
                           ProjeResim = o.ProjeResim,
                           ProjeTarih = o.ProjeTarih

                       }).ToPagedList(sayfa, 6);


            return View(Res);


        }


        public ActionResult YaziDetay(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yazilar yazi = db.Yazilar.Find(id);
            if (yazi == null)
            {
                return HttpNotFound();
            }
            return View(yazi);
        }




        public ActionResult ProjeDetay(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Projeler proje = db.Projeler.Find(id);
            if (proje == null)
            {
                return HttpNotFound();
            }
            return View(proje);
        }



    }
}