﻿using g139010027_Proje.Areas.Admin.Models;
using g139010027_Proje.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace g139010027_Proje.Areas.Admin.Controllers
{
    public class SliderController : Controller
    {
        OtickDbEntities db = new OtickDbEntities();

        // GET: Admin/Slider
        public ActionResult Index()
        {
           var model= db.Yazilar.OrderByDescending(x=>x.YaziID).ToList();
            return View(model);
        }
        public ActionResult Add() {
            
            return View();

        }
        public ActionResult Delete(int id)
        {
            var model=db.Yazilar.Find(id);
            return View(model);

        }
        public ActionResult DeleteConfirm(int id)
        {
            var model = db.Yazilar.Find(id);
            db.Yazilar.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");

        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model=db.Yazilar.Find(id);
            return View(model);

        }
        [HttpPost]
        public ActionResult Edit(SliderModel model)
        {
            if (ModelState.IsValid)
            {



                var bulunan = db.Yazilar.Find(model.YaziID);
                bulunan.YaziBaslik = model.YaziBaslik;
                bulunan.YaziIcerik = model.YaziIcerik;
                bulunan.YaziTarih = model.YaziTarih;
          

                db.SaveChanges();
            }
            //else
            //{
            //    return View(model);
            //}
            return RedirectToAction("Index");

        }
        const string imageFolderPath = "/Content/img/";
        public ActionResult AddSlider(SliderModel model) {
            if (ModelState.IsValid)
            {
                string fileName = string.Empty;
                //dosya kaydetme
                if (model.YaziResim!=null&&model.YaziResim.ContentLength>0)
                {
                     fileName = model.YaziResim.FileName;
                    var path= Path.Combine(Server.MapPath("~" + imageFolderPath),fileName);

                     model.YaziResim.SaveAs(path);
                }
                //ef nesnesi
                Yazilar hb = new Yazilar();
                hb.YaziIcerik = model.YaziIcerik;
                hb.YaziBaslik = model.YaziBaslik;
                hb.YaziTarih = model.YaziTarih;
             
                hb.YaziResim =imageFolderPath + fileName;

                db.Yazilar.Add(hb);
               db.SaveChanges();
            }
            return RedirectToAction("Index");

        }

    }
}