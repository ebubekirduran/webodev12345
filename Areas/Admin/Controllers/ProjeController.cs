﻿using g139010027_Proje.Areas.Admin.Models;
using g139010027_Proje.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace g139010027_Proje.Areas.Admin.Controllers
{
    public class ProjeController : Controller
    {
        OtickDbEntities db = new OtickDbEntities();

        // GET: Admin/Slider
        public ActionResult Index()
        {
           var model= db.Projeler.OrderByDescending(x=>x.ProjeID).ToList();
            return View(model);
        }
        public ActionResult Add() {
            
            return View();

        }
        public ActionResult Delete(int id)
        {
            var model=db.Projeler.Find(id);
            return View(model);

        }
        public ActionResult DeleteConfirm(int id)
        {
            var model = db.Projeler.Find(id);
            db.Projeler.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");

        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model=db.Projeler.Find(id);
            return View(model);

        }
        [HttpPost]
        public ActionResult Edit(ProjeModel model)
        {
            if (ModelState.IsValid)
            {



                var bulunan = db.Projeler.Find(model.ProjeID);
                bulunan.ProjeAdi = model.ProjeAdi;
                bulunan.ProjeAciklama = model.ProjeAciklama;
                bulunan.ProjeTarih = model.ProjeTarih;
          

                db.SaveChanges();
            }
            //else
            //{
            //    return View(model);
            //}
            return RedirectToAction("Index");

        }
        const string imageFolderPath = "/Content/img/";
        public ActionResult AddProje(ProjeModel model) {
            if (ModelState.IsValid)
            {
                string fileName = string.Empty;
                //dosya kaydetme
                if (model.ProjeResim!=null&&model.ProjeResim.ContentLength>0)
                {
                     fileName = model.ProjeResim.FileName;
                    var path= Path.Combine(Server.MapPath("~" + imageFolderPath),fileName);

                     model.ProjeResim.SaveAs(path);
                }
                //ef nesnesi
                Projeler hb = new Projeler();
                hb.ProjeAciklama = model.ProjeAciklama;
                hb.ProjeAdi = model.ProjeAdi;
                hb.ProjeTarih = model.ProjeTarih;
             
                hb.ProjeResim =imageFolderPath + fileName;

                db.Projeler.Add(hb);
               db.SaveChanges();
            }
            return RedirectToAction("Index");

        }

    }
}