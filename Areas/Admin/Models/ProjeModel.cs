﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace g139010027_Proje.Areas.Admin.Models
{
    public class ProjeModel
    {
        public int ProjeID { get; set; }
        public string ProjeAdi { get; set; }
        public string ProjeAciklama { get; set; }
        public System.DateTime ProjeTarih { get; set; }
   
        public HttpPostedFileBase ProjeResim { get; set; }
    
    }
}