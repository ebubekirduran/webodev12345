﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace g139010027_Proje.Areas.Admin.Models
{
    public class SliderModel
    {
        public int YaziID { get; set; }
        public string YaziBaslik { get; set; }
        public string YaziIcerik { get; set; }
        public System.DateTime YaziTarih { get; set; }
   
        public HttpPostedFileBase YaziResim { get; set; }
    
    }
}