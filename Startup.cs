﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(g139010027_Proje.Startup))]
namespace g139010027_Proje
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
